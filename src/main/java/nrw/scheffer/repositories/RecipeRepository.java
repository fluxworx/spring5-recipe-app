package nrw.scheffer.repositories;

import org.springframework.data.repository.CrudRepository;

import nrw.scheffer.domain.Recipe;

public interface RecipeRepository extends CrudRepository<Recipe, Long> {

}
