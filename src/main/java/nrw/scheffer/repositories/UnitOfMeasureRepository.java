package nrw.scheffer.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import nrw.scheffer.domain.UnitOfMeasure;

public interface UnitOfMeasureRepository extends CrudRepository<UnitOfMeasure, Long> {
	public Optional<UnitOfMeasure> findByDescription(String description);
}
