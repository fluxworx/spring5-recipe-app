package nrw.scheffer.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import nrw.scheffer.domain.Category;

public interface CategoryRepository extends CrudRepository<Category, Long> {

	public Optional<Category> findByDescription(String description);
}
