package nrw.scheffer.services;

import java.io.IOException;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import lombok.extern.slf4j.Slf4j;
import nrw.scheffer.domain.Recipe;
import nrw.scheffer.repositories.RecipeRepository;

@Slf4j
@Service
public class ImageServiceImpl implements ImageService {

	RecipeRepository recipeRepository;

	public ImageServiceImpl(RecipeRepository recipeRepository) {
		this.recipeRepository = recipeRepository;
	}

	@Override
	@Transactional
	public void saveImageFile(Long recipeId, MultipartFile file) {

		try {
			Recipe recipe = recipeRepository.findById(recipeId).get();

			Byte[] byteObjects = convertByteArray(file);

			recipe.setImage(byteObjects);

			recipeRepository.save(recipe);
		} catch (IOException e) {
			// todo handle better
			log.error("Error occurred", e);

			e.printStackTrace();
		}
	}

	private Byte[] convertByteArray(MultipartFile file) throws IOException {
		Byte[] byteObjects = new Byte[file.getBytes().length];

		int i = 0;

		for (byte b : file.getBytes()) {
			byteObjects[i++] = b;
		}
		return byteObjects;
	}

}
