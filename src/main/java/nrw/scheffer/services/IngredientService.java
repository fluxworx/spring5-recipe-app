package nrw.scheffer.services;

import nrw.scheffer.commands.IngredientCommand;

public interface IngredientService {

	IngredientCommand findByRecipeAndIngredientId(Long recipeId, Long id);

	IngredientCommand saveIngredientCommand(IngredientCommand command);

	void deleteByRecipeAndIngredientId(Long recipeId, Long id);
}
