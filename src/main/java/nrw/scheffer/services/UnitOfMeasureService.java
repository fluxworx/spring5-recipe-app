package nrw.scheffer.services;

import java.util.Set;

import nrw.scheffer.commands.UnitOfMeasureCommand;

public interface UnitOfMeasureService {

	Set<UnitOfMeasureCommand> listAllUoms();

}
