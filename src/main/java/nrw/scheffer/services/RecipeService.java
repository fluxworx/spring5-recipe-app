package nrw.scheffer.services;

import java.util.Set;

import nrw.scheffer.commands.RecipeCommand;
import nrw.scheffer.domain.Recipe;

public interface RecipeService {

	Set<Recipe> getRecipes();

	Recipe findById(Long id);

	RecipeCommand saveRecipeCommand(RecipeCommand command);

	RecipeCommand findCommandById(Long id);

	void deleteById(Long id);
}
