package nrw.scheffer.boostrap;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import nrw.scheffer.domain.Category;
import nrw.scheffer.domain.Difficulty;
import nrw.scheffer.domain.Ingredient;
import nrw.scheffer.domain.Notes;
import nrw.scheffer.domain.Recipe;
import nrw.scheffer.domain.UnitOfMeasure;
import nrw.scheffer.repositories.CategoryRepository;
import nrw.scheffer.repositories.RecipeRepository;
import nrw.scheffer.repositories.UnitOfMeasureRepository;

@Slf4j
@Component
@Profile("default")
public class RecipeBootstrap implements ApplicationListener<ContextRefreshedEvent> {

	private RecipeRepository recipeRepository;
	private CategoryRepository categoryRepository;
	private UnitOfMeasureRepository unitOfMeasureRepository;

	public RecipeBootstrap(RecipeRepository recipeRepository, CategoryRepository categoryRepository,
			UnitOfMeasureRepository unitOfMeasureRepository) {
		this.recipeRepository = recipeRepository;
		this.categoryRepository = categoryRepository;
		this.unitOfMeasureRepository = unitOfMeasureRepository;
	}

	@Override
	@Transactional
	public void onApplicationEvent(ContextRefreshedEvent event) {
		recipeRepository.saveAll(initData());

		log.debug("Context refreshed.");
	}

	private Set<Recipe> initData() {

		Map<String, Category> categories = new HashMap<String, Category>();
		Iterable<Category> itCategories = categoryRepository.findAll();
		for (Category category : itCategories) {
			categories.put(category.getDescription(), category);
		}

		Map<String, UnitOfMeasure> units = new HashMap<String, UnitOfMeasure>();
		Iterable<UnitOfMeasure> it = unitOfMeasureRepository.findAll();
		for (UnitOfMeasure unitOfMeasure : it) {
			units.put(unitOfMeasure.getDescription(), unitOfMeasure);
		}

		Set<Recipe> recipes = new HashSet<Recipe>();

		// Guacamole
		recipes.add(addGuacamoleRecipe(categories, units));

		// Taco
		recipes.add(addTacoRecipe(categories, units));

		return recipes;
	}

	private Recipe addGuacamoleRecipe(Map<String, Category> categories, Map<String, UnitOfMeasure> units) {

		Recipe recipe = new Recipe();

		recipe.setDescription("How to Make Perfect Guacamole");
		recipe.setCookTime(10);
		recipe.setPrepTime(0);
		recipe.setDifficulty(Difficulty.EASY);
		recipe.setDirections(
				"1 Cut avocado, remove flesh: Cut the avocados in half. Remove seed. Score the inside of the avocado with a blunt knife and scoop out the flesh with a spoon. (See How to Cut and Peel an Avocado.) Place in a bowl.\r\n"
						+ "2 Mash with a fork: Using a fork, roughly mash the avocado. (Don't overdo it! The guacamole should be a little chunky.)\r\n"
						+ "3 Add salt, lime juice, and the rest: Sprinkle with salt and lime (or lemon) juice. The acid in the lime juice will provide some balance to the richness of the avocado and will help delay the avocados from turning brown.\r\n"
						+ "Add the chopped onion, cilantro, black pepper, and chiles. Chili peppers vary individually in their hotness. So, start with a half of one chili pepper and add to the guacamole to your desired degree of hotness.\r\n"
						+ "Remember that much of this is done to taste because of the variability in the fresh ingredients. Start with this recipe and adjust to your taste.\r\n"
						+ "4 Cover with plastic and chill to store: Place plastic wrap on the surface of the guacamole cover it and to prevent air reaching it. (The oxygen in the air causes oxidation which will turn the guacamole brown.) Refrigerate until ready to serve.\r\n"
						+ "Chilling tomatoes hurts their flavor, so if you want to add chopped tomato to your guacamole, add it just before serving.\r\n"
						+ "Read more: http://www.simplyrecipes.com/recipes/perfect_guacamole/#ixzz4sTZOrEYQ\r\n");

		Notes notes = new Notes();
		notes.setRecipeNotes(
				"For a very quick guacamole just take a 1/4 cup of salsa and mix it in with your mashed avocados.\r\n"
						+ "Feel free to experiment! One classic Mexican guacamole has pomegranate seeds and chunks of peaches in it (a Diana Kennedy favorite). Try guacamole with added pineapple, mango, or strawberries.\r\n"
						+ "The simplest version of guacamole is just mashed avocados with salt. Don't let the lack of availability of other ingredients stop you from making guacamole.\r\n"
						+ "To extend a limited supply of avocados, add either sour cream or cottage cheese to your guacamole dip. Purists may be horrified, but so what? It tastes great.\r\n"
						+ "For a deviled egg version with guacamole, try our Guacamole Deviled Eggs!\r\n"
						+ "Read more: http://www.simplyrecipes.com/recipes/perfect_guacamole/#ixzz4sTcp4GKa\r\n");
		recipe.setNotes(notes);

		recipe.addIngredient(new Ingredient("ripe avocado", new BigDecimal(2), units.get("Piece")));
		recipe.addIngredient(new Ingredient("Kosher salt", new BigDecimal(0.5), units.get("Teaspoon")));
		recipe.addIngredient(
				new Ingredient("fresh lime juice or lemon juice", new BigDecimal(1), units.get("Tablespoon")));
		recipe.addIngredient(new Ingredient("minced red onion or thinly sliced green onion", new BigDecimal(2),
				units.get("Tablespoon")));
		recipe.addIngredient(new Ingredient("serrano chiles, stems and seeds removed, minced", new BigDecimal(2),
				units.get("Piece")));
		recipe.addIngredient(new Ingredient("cilantro (leaves and tender stems), finely chopped", new BigDecimal(2),
				units.get("Tablespoon")));
		recipe.addIngredient(new Ingredient("freshly grated black pepper", new BigDecimal(1), units.get("Dash")));
		recipe.addIngredient(new Ingredient("ripe tomato, seeds and pulp removed, chopped", new BigDecimal(0.5),
				units.get("Piece")));

		recipe.getCategories().add(categories.get("American"));
		recipe.getCategories().add(categories.get("Mexican"));

		recipe.setUrl("http://www.simplyrecipes.com/recipes/perfect_guacamole/");
		recipe.setServings(4);
		recipe.setSource("Simple Recipe");

		return recipe;
	}

	private Recipe addTacoRecipe(Map<String, Category> categories, Map<String, UnitOfMeasure> units) {

		Recipe recipe = new Recipe();

		recipe.setDescription("Spicy Grilled Chicken Tacos");
		recipe.setCookTime(20);
		recipe.setPrepTime(30);
		recipe.setDifficulty(Difficulty.HARD);
		recipe.setDirections("1 Prepare a gas or charcoal grill for medium-high, direct heat.\r\n"
				+ "2 Make the marinade and coat the chicken: In a large bowl, stir together the chili powder, oregano, cumin, sugar, salt, garlic and orange zest. Stir in the orange juice and olive oil to make a loose paste. Add the chicken to the bowl and toss to coat all over.\r\n"
				+ "Set aside to marinate while the grill heats and you prepare the rest of the toppings.\r\n"
				+ "3 Grill the chicken: Grill the chicken for 3 to 4 minutes per side, or until a thermometer inserted into the thickest part of the meat registers 165F. Transfer to a plate and rest for 5 minutes.\r\n"
				+ "4 Warm the tortillas: Place each tortilla on the grill or on a hot, dry skillet over medium-high heat. As soon as you see pockets of the air start to puff up in the tortilla, turn it with tongs and heat for a few seconds on the other side.\r\n"
				+ "Wrap warmed tortillas in a tea towel to keep them warm until serving.\r\n"
				+ "5 Assemble the tacos: Slice the chicken into strips. On each tortilla, place a small handful of arugula. Top with chicken slices, sliced avocado, radishes, tomatoes, and onion slices. Drizzle with the thinned sour cream. Serve with lime wedges.\r\n"
				+ "");

		recipe.addIngredient(new Ingredient("ancho chili powder", new BigDecimal(2), units.get("Tablespoon")));
		recipe.addIngredient(new Ingredient("dried orgena", new BigDecimal(1), units.get("Teaspoon")));
		recipe.addIngredient(new Ingredient("dried cumin", new BigDecimal(1), units.get("Teaspoon")));
		recipe.addIngredient(new Ingredient("sugar", new BigDecimal(1), units.get("Teaspoon")));
		recipe.addIngredient(new Ingredient("salt", new BigDecimal(0.5), units.get("Teaspoon")));
		recipe.addIngredient(new Ingredient("garlic, finely chopped", new BigDecimal(1), units.get("Glove")));
		recipe.addIngredient(new Ingredient("finely grated orange zest", new BigDecimal(1), units.get("Tablespoon")));
		recipe.addIngredient(new Ingredient("fresh-squeezed orange juice", new BigDecimal(3), units.get("Tablespoon")));
		recipe.addIngredient(new Ingredient("olive oil", new BigDecimal(2), units.get("Tablespoon")));
		recipe.addIngredient(
				new Ingredient("skinless, boneless chicken thighs", new BigDecimal(5), units.get("Piece")));

		recipe.getCategories().add(categories.get("American"));

		recipe.setUrl("http://www.simplyrecipes.com/recipes/spicy_grilled_chicken_tacos/");
		recipe.setServings(4);
		recipe.setSource("Simple Recipe");

		return recipe;
	}

}
