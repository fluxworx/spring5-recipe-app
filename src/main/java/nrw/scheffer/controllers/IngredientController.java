package nrw.scheffer.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import lombok.extern.slf4j.Slf4j;
import nrw.scheffer.commands.IngredientCommand;
import nrw.scheffer.commands.RecipeCommand;
import nrw.scheffer.commands.UnitOfMeasureCommand;
import nrw.scheffer.services.IngredientService;
import nrw.scheffer.services.RecipeService;
import nrw.scheffer.services.UnitOfMeasureService;

/**
 * Created by jt on 6/28/17.
 */
@Slf4j
@Controller
public class IngredientController {

	private final RecipeService recipeService;
	private final IngredientService ingredientService;
	private final UnitOfMeasureService unitOfMeasureService;

	public IngredientController(RecipeService recipeService, IngredientService ingredientService,
			UnitOfMeasureService unitOfMeasureService) {
		this.recipeService = recipeService;
		this.ingredientService = ingredientService;
		this.unitOfMeasureService = unitOfMeasureService;
	}

	@GetMapping("/recipe/{recipeId}/ingredient/{id}/show")
	public String showById(@PathVariable String recipeId, @PathVariable String id, Model model) {

		model.addAttribute("ingredient",
				ingredientService.findByRecipeAndIngredientId(new Long(recipeId), new Long(id)));

		return "recipe/ingredient/show";
	}

	@GetMapping("/recipe/{recipeId}/ingredients")
	public String listIngredients(@PathVariable String recipeId, Model model) {
		log.debug("Getting ingredient list for recipe id: " + recipeId);

		// use command object to avoid lazy load errors in Thymeleaf.
		model.addAttribute("recipe", recipeService.findCommandById(Long.valueOf(recipeId)));

		return "recipe/ingredient/list";
	}

	@GetMapping("/recipe/{recipeId}/ingredient/new")
	public String newIngredient(@PathVariable String recipeId, Model model) {

		RecipeCommand recipeCommand = recipeService.findCommandById(new Long(recipeId));
		// todo reaise Exception if null

		IngredientCommand ingredientCommand = new IngredientCommand();
		ingredientCommand.setRecipeId(new Long(recipeId));
		ingredientCommand.setUnitOfMeasure(new UnitOfMeasureCommand());

		model.addAttribute("ingredient", ingredientCommand);
		model.addAttribute("uomList", unitOfMeasureService.listAllUoms());

		return "recipe/ingredient/ingredientform";
	}

	@GetMapping("/recipe/{recipeId}/ingredient/{id}/update")
	public String updateIngredient(@PathVariable String recipeId, @PathVariable String id, Model model) {

		model.addAttribute("ingredient",
				ingredientService.findByRecipeAndIngredientId(new Long(recipeId), new Long(id)));

		model.addAttribute("uomList", unitOfMeasureService.listAllUoms());

		return "recipe/ingredient/ingredientform";
	}

	@PostMapping("/recipe/{recipeId}/ingredient")
	public String saveOrUpdate(@ModelAttribute IngredientCommand command) {
		IngredientCommand savedCommand = ingredientService.saveIngredientCommand(command);

		log.debug(String.format("saved recipe id: %s", savedCommand.getRecipeId()));
		log.debug(String.format("saved ingredient id: %s", savedCommand.getId()));

		return String.format("redirect:/recipe/%s/ingredient/%s/show", savedCommand.getRecipeId(),
				savedCommand.getId());
	}

	@GetMapping("/recipe/{recipeId}/ingredient/{id}/delete")
	public String deleteIngredient(@PathVariable String recipeId, @PathVariable String id, Model model) {

		ingredientService.deleteByRecipeAndIngredientId(new Long(recipeId), new Long(id));

		return String.format("redirect:/recipe/%s/ingredients", recipeId);
	}
}
