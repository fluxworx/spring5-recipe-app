package nrw.scheffer.domain;

public enum Difficulty {

	EASY, MODERATE, HARD
}
