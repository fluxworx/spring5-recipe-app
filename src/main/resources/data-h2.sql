insert into category (description) values 
	('American'),
	('Italian'),
	('Mexican'),
	('Fast Food')
;

insert into unit_of_measure (description) values 
	('Teaspoon'),
	('Tablespoon'),
	('Cup'),
	('Pinch'),
	('Ounce'),
	('Piece'),
	('Dash'),
	('Glove')
;