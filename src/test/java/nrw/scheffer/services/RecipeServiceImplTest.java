package nrw.scheffer.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import nrw.scheffer.converters.RecipeCommandToRecipe;
import nrw.scheffer.converters.RecipeToRecipeCommand;
import nrw.scheffer.domain.Recipe;
import nrw.scheffer.exceptions.NotFoundException;
import nrw.scheffer.repositories.RecipeRepository;

/**
 * Created by jt on 6/17/17.
 */
public class RecipeServiceImplTest {

	private RecipeService recipeService;

	@Mock
	private RecipeRepository recipeRepository;

	@Mock
	private RecipeToRecipeCommand recipeToRecipeCommand;

	@Mock
	private RecipeCommandToRecipe recipeCommandToRecipe;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		recipeService = new RecipeServiceImpl(recipeRepository, recipeCommandToRecipe, recipeToRecipeCommand);
	}

	@Test(expected = NotFoundException.class)
	public void getRecipeIdTestNotFound() throws Exception {

		Optional<Recipe> recipeOptional = Optional.empty();

		when(recipeRepository.findById(anyLong())).thenReturn(recipeOptional);

		recipeService.findById(1L);

		// should fail
	}

	@Test
	public void getRecipeByIdTest() throws Exception {
		Recipe recipe = new Recipe();
		recipe.setId(1L);
		Optional<Recipe> recipeOptional = Optional.of(recipe);

		when(recipeRepository.findById(anyLong())).thenReturn(recipeOptional);
		Recipe recipeReturned = recipeService.findById(1L);

		assertNotNull("Null recipe returned", recipeReturned);
		verify(recipeRepository, times(1)).findById(anyLong());
		verify(recipeRepository, never()).findAll();
	}

	@Test
	public void getRecipesTest() throws Exception {

		Recipe recipe = new Recipe();
		HashSet<Recipe> receipesData = new HashSet<Recipe>();
		receipesData.add(recipe);

		when(recipeService.getRecipes()).thenReturn(receipesData);
		Set<Recipe> recipes = recipeService.getRecipes();

		assertEquals(recipes.size(), 1);
		verify(recipeRepository, times(1)).findAll();
		verify(recipeRepository, never()).findById(anyLong());
	}

	@Test
	public void testDeleteById() throws Exception {

		// given
		Long idToDelete = Long.valueOf(2L);

		// when
		recipeService.deleteById(idToDelete);

		// then
		verify(recipeRepository, times(1)).deleteById(anyLong());

	}

}