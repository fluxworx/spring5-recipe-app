# Spring Boot Recipe Application

[![pipeline status](https://gitlab.com/fluxworx/spring5-recipe-app/badges/master/pipeline.svg)](https://gitlab.com/fluxworx/spring5-recipe-app/commits/master)

[![coverage report](https://gitlab.com/fluxworx/spring5-recipe-app/badges/master/coverage.svg)](https://gitlab.com/fluxworx/spring5-recipe-app/commits/master)

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/pantomath-io/demo-tools)](https://goreportcard.com/report/gitlab.com/pantomath-io/demo-tools)

[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)



This repository is for an example application built in my Spring Framework 5 - Beginner to Guru

You can learn about my Spring Framework 5 Online course [here.](http://courses.springframework.guru/p/spring-framework-5-begginer-to-guru/?product_id=363173)